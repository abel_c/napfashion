//
//  Mocks.swift
//  NAPFashionTests
//
//  Created by Abel Castro on 14/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation
import UIKit
@testable import NAPFashion

class NAPMainNavigatorMock : NAPMainNavigatorProtocol {
  var mainNavigationController: UINavigationController = UINavigationController()
  
  var setup: Bool = false
  init() {}
  
  func setupPlaceholder() {
    setup = true
  }

}

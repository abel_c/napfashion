//
//  NAPMainManagerTests.swift
//  NAPFashionTests
//
//  Created by Abel Castro on 9/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import XCTest
@testable import NAPFashion

class NAPMainManagerTests: XCTestCase {
  
  var manager: NAPMainManagerProtocol!
  var navigator: NAPMainNavigatorMock!
  
  override func setUp() {
    super.setUp()
    navigator = NAPMainNavigatorMock()
    manager = NAPMainManager(navigator: navigator)
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func testStart() {
    // When
    manager.start()
    
    // Then
    XCTAssertTrue(navigator.setup, "set up should have been called")
    
  }
  
}

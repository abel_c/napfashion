//
//  NAPMainNavigatorTests.swift
//  NAPFashionTests
//
//  Created by Abel Castro on 14/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import XCTest
@testable import NAPFashion

class NAPMainNavigatorTests: XCTestCase {
  
  fileprivate var navigationController = UINavigationControllerExtended()
  fileprivate var navigator: NAPMainNavigatorProtocol!
  
  override func setUp() {
    super.setUp()
    navigator = NAPMainNavigator(mainNavigationController: navigationController)
  }
  func test_setupPlaceholder() {
    // When
    navigator.setupPlaceholder()
    // Then
    XCTAssertTrue(navigationController.viewControllersSet, "View controllers should have been set")
  }
}

fileprivate class UINavigationControllerExtended : UINavigationController {
  var viewControllersSet = false
  override func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
    viewControllersSet = true
    super.setViewControllers(viewControllers, animated: animated)
  }
}

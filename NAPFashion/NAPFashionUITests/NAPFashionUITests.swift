//
//  NAPFashionUITests.swift
//  NAPFashionUITests
//
//  Created by Abel Castro on 9/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import XCTest

class NAPFashionUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
      
      let collectionView = XCUIApplication().otherElements.containing(.navigationBar, identifier:"NAPClothing.NAPCHomeView").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .collectionView).element
      guard collectionView.staticTexts["item0"].waitForExistence(timeout: 4) else { return }
      XCTAssertEqual(collectionView.cells.element(boundBy: 0).staticTexts.element(boundBy: 0).label, "item0")
      XCTAssertEqual(collectionView.cells.element(boundBy: 0).staticTexts.element(boundBy: 1).label, "£400.00")
      XCTAssertEqual(collectionView.cells.element(boundBy: 1).staticTexts.element(boundBy: 0).label, "item1")
      XCTAssertEqual(collectionView.cells.element(boundBy: 1).staticTexts.element(boundBy: 1).label, "£400.00")
     
      let favouriteButton = collectionView.cells.element(boundBy: 0).buttons.element(boundBy: 0)
      XCTAssertEqual(favouriteButton.label, "heart1")
      favouriteButton.tap()
      XCTAssertEqual(favouriteButton.label, "heart2")
    }
    
}

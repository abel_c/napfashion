//
//  NAPNetworkManager.swift
//  NAPFashion
//
//  Created by Abel Castro on 9/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import Foundation
import NAPNetwork

class NAPNetManager {
  
  var itemsPerPage: UInt = 100
  var offset: Int = 0
  var defaultEndpoint: String = "https://api.net-a-porter.com/NAP/GB/en"
  // "/60/0/summaries?categoryIds=2"
  
  init() {
    NAPNetworkManager.sharedInstance.defaultEndpoint = defaultEndpoint
    NAPNetworkManager.sharedInstance.itemsPerPage = itemsPerPage
    NAPNetworkManager.sharedInstance.offset = offset
  }
  
}


//
//  NAPMainNavigator.swift
//  NAPFashion
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import UIKit

protocol NAPMainNavigatorProtocol {
  var mainNavigationController: UINavigationController {get}
  func setupPlaceholder()
}

class NAPMainNavigator : NAPMainNavigatorProtocol {
  
  let mainNavigationController: UINavigationController
  
  init(mainNavigationController: UINavigationController = UINavigationController()) {
    self.mainNavigationController = mainNavigationController
  }
  
  func setupPlaceholder() {
    let placeholderViewController = UIViewController()
    placeholderViewController.view.backgroundColor = .white
    mainNavigationController.setViewControllers([placeholderViewController], animated: false)
  }
}

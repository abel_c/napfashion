//
//  AppDelegate.swift
//  NAPFashion
//
//  Created by Abel Castro on 9/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  fileprivate var manager: NAPMainManagerProtocol!
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    window = UIWindow()
    manager = NAPMainManager()
    manager.setUp(window: window)
    manager.start()
    
    return true
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    manager.willTerminate()
  }
  
  
}


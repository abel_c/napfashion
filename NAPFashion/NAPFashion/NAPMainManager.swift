//
//  NAPMainManager.swift
//  NAPFashion
//
//  Created by Abel Castro on 10/1/18.
//  Copyright © 2018 Abel Castro. All rights reserved.
//

import UIKit
import NAPNetwork
import NAPClothing

protocol NAPMainManagerProtocol {
  func setUp(window: UIWindow?)
  func start()
  func willTerminate()
}
final class NAPMainManager : NAPMainManagerProtocol {
  
  var navigator: NAPMainNavigatorProtocol
  let networkManager: NAPNetManager
  
  // Different pieces of the app
  // [It can have its own manager]
  let clothingManager: NAPClothingManager
  
  init(navigator: NAPMainNavigatorProtocol = NAPMainNavigator()) {
    self.navigator = navigator
    networkManager = NAPNetManager()
    clothingManager = NAPClothingManager(navigationController: navigator.mainNavigationController)
  }
  
  func setUp(window: UIWindow?) {
    window?.rootViewController = navigator.mainNavigationController
    window?.makeKeyAndVisible()
  }
  
  func start() {
    navigator.setupPlaceholder()
    clothingManager.start()
  }
  
  func willTerminate() {
    NAPImageCacheManager().flushCache()
    NAPPersistItemManager.sharedInstance.saveFavourites()
  }
}
